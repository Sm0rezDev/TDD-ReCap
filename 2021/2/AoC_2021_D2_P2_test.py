from src.AoC_2021_D2_P2 import move

def test_move():
    command_set = ['forward 5', 'down 5', 'forward 8', 'up 3', 'down 8', 'forward 2']
    expected_cord = (15, 60)
    
    command_set = [(_.split()[0], _.split()[1]) for _ in command_set]

    x, y, z = 0, 0, 0
    for cmd in command_set:
        x, y, z = move(cmd, x, y, z)
    
    assert (x, y) == expected_cord

test_move()