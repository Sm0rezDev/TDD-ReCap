from src.AoC_2021_D2_P1 import move

def test_move():
	command_set = ['forward 5', 'down 5', 'forward 8', 'up 3', 'down 8', 'forward 2']
	expected_cord = (15, 10)

	command_set = [(_.split()[0], _.split()[1]) for _ in command_set]

	x, y = 0, 0
	for cmd in command_set:
		x, y = move(cmd, x, y)

	assert (x, y) == expected_cord
