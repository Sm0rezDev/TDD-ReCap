
def load(file_in: str):
	"""
	load() returns file content
	"""
	try:
		with open(file_in) as file:
			file = file.read()
			return file.splitlines()
	except FileNotFoundError:
			print("Can't find the file")


def move(command: str, x: int, y: int):
	"""
	move() given command will do operation to the current cordinates values.
	"""

	cmd, step = command

	if cmd == 'forward':
		x += int(step)
	if cmd == 'down':
		y += int(step)
	if cmd == 'up':
		y -= int(step)

	return x, y


def main():
	data = load('2021/2/.input')
	
	x, y = 0, 0
	command_set = [(_.split()[0], _.split()[1]) for _ in data]
	for cmd in command_set:
		x, y = move(cmd, x, y)
	results = x*y
	
	print(results)


if __name__ == '__main__':
	main()
