
def load(file_in: str):
	"""
	load() returns file content
	"""
	try:
		with open(file_in) as file:
			file = file.read()
			return file.splitlines()
	except FileNotFoundError:
			print("Can't find the file")


def move(command: str, x: int, y: int, z: int):
    """
    move() given command will do operation to the current cordinates values.
    """
    
    cmd, step = command

    if cmd == 'forward':
        x += int(step)
        y += z * int(step)
    if cmd == 'down':
        z += int(step)
    if cmd == 'up':
        z -= int(step)
    
    return x, y, z


def main():
	data = load('2021/2/.input')
	
	x, y, z = 0, 0, 0
	command_set = [(_.split()[0], _.split()[1]) for _ in data]
	for cmd in command_set:
		x, y , z= move(cmd, x, y, z)
	results = x*y
	
	print(results)


if __name__ == '__main__':
	main()
