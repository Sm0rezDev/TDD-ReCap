from src.AoC_2021_D1_P1 import isIncreased

test_data = [ 199, 200, 208, 210, 200 ]
expected = [ None, True, True, True, False]

def test_increased():
    """
    Test if depth is increased or not.\n
    """

    for idx, depth in enumerate(test_data):
        print(depth, expected[idx])
        assert isIncreased(test_data, idx) ==  expected[idx]
