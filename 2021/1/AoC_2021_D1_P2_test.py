from src.AoC_2021_D1_P2 import isIncreased, sumWindow

input_data = [ 199, 200, 208, 210, 200, 207, 240, 269, 260, 263 ]
input_sum  = [ 607, 618, 618, 617, 647, 716, 769, 792 ]
expected   = [ None, True, 'NO CHANGE', False, True, True, True, True ]


def test_windowSum():
    assert sumWindow(input_data) == input_sum


def test_increased():
    for i in range(len(input_sum)):
        assert isIncreased(input_sum, i) == expected[i]
