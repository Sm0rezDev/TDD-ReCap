
def isIncreased(depth, idx):
    """
    Test values if value have been increased or not.
    Returns None if there was no previous data to compare
    """

    increased = depth[idx] > depth[idx - 1]

    if idx != 0:
        if increased:
            return True
        else:
            return False
    else:
        return None


def main():
    
    with open('2021/1/.input') as f:
        someInput = f.read()

    someInput = someInput.splitlines()

    data = []
    for elem in someInput:
        data.append(int(elem))
        
    result = 0

    for i in range(0, len(data)):

        increased = isIncreased(data, i)

        if increased is not None:
            if increased:
                print(f'{data[i]} (increased)')
                result += 1
            elif not increased:
                print(f'{data[i]} (decreased)')
        else:
            print(f'{data[i]} (N/A - no previous measurement)')

    print(f'----------------')
    print(f'Answer: {result}')


# Answer: 1529
if __name__ == '__main__':
    main()
