
def sumWindow(data):
    _mem = []
    for i in range(len(data) - 2):
        window_sum = sum(data[i:i+3])
        _mem.append(window_sum)
    return _mem


def isIncreased(val, idx):
    """
    Test values if value have been increased or not.
    Returns None if there was no previous data to compare
    """
    increased = val[idx] > val[idx - 1]
    equal     = val[idx] == val[idx - 1]

    if idx != 0:
        if increased:
            return True
        if equal:
            return 'NO CHANGE'
        elif not increased:
            return False
    else:
        return None

def getResult(sums):
    result = 0
    for i, val in enumerate(sums):

        increased = sums[i] > sums[i - 1]
        equal     = sums[i] == sums[i - 1]

        if i != 0:
            if increased:
                result += 1
                print(f'{val} (increased)')
            elif equal:
                result == 0
                print(f'{val} (no change)')
            else:
                print(f'{val} (decreased)')
        else:
            print(f'{val} (N/A - no previous sum)')
    return result


def main():
    
    with open('2021/1/.input') as f:
        someInput = f.read()

    someInput = someInput.splitlines()

    data = []
    for elem in someInput:
        data.append(int(elem))
        
    _sum = sumWindow(data)

    answer = getResult(_sum)
    print(f'----------------')
    print(f'Answer: {answer}')

# Answer: 1529
if __name__ == '__main__':
    main()
